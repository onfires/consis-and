package com.er1ck.consis.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.er1ck.consis.service.PersonContract;
import com.er1ck.consis.service.PersonContract.PersonEntry;

/**
 * Created by aplicacionesmoviles on 19/06/17.
 */

public class PersonService {
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PersonEntry.TABLE_NAME + " (" +
                    PersonEntry._ID + " INTEGER PRIMARY KEY," +
                    PersonEntry.COLUMN_NAME_NAME      + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_LAST_NAME + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_DIRECTION + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_TEL       + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_EMAIL     + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_BIRTHDAY  + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_STATE     + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_NICKNAME  + TEXT_TYPE + COMMA_SEP +
                    PersonEntry.COLUMN_NAME_PASSWORD  + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PersonEntry.TABLE_NAME;

    public static class PersonReaderDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "PersonReader.db";

        public PersonReaderDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_CREATE_ENTRIES);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }


    }


}
