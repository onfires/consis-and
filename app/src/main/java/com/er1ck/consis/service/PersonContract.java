package com.er1ck.consis.service;

import android.provider.BaseColumns;

/**
 * Created by aplicacionesmoviles on 19/06/17.
 */

public final class PersonContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private PersonContract() {}

    /* Inner class that defines the table contents */
    public static class PersonEntry implements BaseColumns {
        public static final String TABLE_NAME = "person_entry";

        public static final String COLUMN_NAME_NAME         = "name";
        public static final String COLUMN_NAME_LAST_NAME    = "last_name";
        public static final String COLUMN_NAME_DIRECTION    = "direction";
        public static final String COLUMN_NAME_TEL          = "tel";
        public static final String COLUMN_NAME_EMAIL        = "email";
        public static final String COLUMN_NAME_BIRTHDAY     = "birthday";
        public static final String COLUMN_NAME_STATE        = "state";
        public static final String COLUMN_NAME_NICKNAME     = "nickname";
        public static final String COLUMN_NAME_PASSWORD     = "password";
    }
}

