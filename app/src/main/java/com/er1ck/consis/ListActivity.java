package com.er1ck.consis;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.er1ck.consis.adapter.PersonAdapter;
import com.er1ck.consis.entity.Person;
import com.er1ck.consis.service.PersonContract;
import com.er1ck.consis.service.PersonService;

public class ListActivity extends AppCompatActivity implements View.OnClickListener  {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private PersonService.PersonReaderDbHelper mDbHelper;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);



        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);




        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //get Perosn Data
        this.mDbHelper = new PersonService.PersonReaderDbHelper(this);
        Cursor c = readPersons();

        // specify an adapter (see also next example)
        mAdapter = new PersonAdapter(c);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {

        if (v == fab){
            Intent intent = new Intent(this, FormActivity.class);
            startActivity(intent);
        }
    }

    private Cursor readPersons(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                PersonContract.PersonEntry._ID,
                PersonContract.PersonEntry.COLUMN_NAME_NAME,
                PersonContract.PersonEntry.COLUMN_NAME_LAST_NAME,
                PersonContract.PersonEntry.COLUMN_NAME_DIRECTION,
                PersonContract.PersonEntry.COLUMN_NAME_TEL,
                PersonContract.PersonEntry.COLUMN_NAME_EMAIL,
                PersonContract.PersonEntry.COLUMN_NAME_BIRTHDAY,
                PersonContract.PersonEntry.COLUMN_NAME_STATE,
                PersonContract.PersonEntry.COLUMN_NAME_NICKNAME,
                PersonContract.PersonEntry.COLUMN_NAME_PASSWORD,
        };

        // Filter results WHERE "title" = 'My Title'
        //String selection = PersonEntry.COLUMN_NAME_TITLE + " = ?";
        //String[] selectionArgs = { "My Title" };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                PersonContract.PersonEntry._ID + " ASC";

        Cursor c = db.query(
                PersonContract.PersonEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,//selection,                                // The columns for the WHERE clause
                null,//selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return c;
    }


}
