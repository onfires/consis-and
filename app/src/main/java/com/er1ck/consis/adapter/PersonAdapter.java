package com.er1ck.consis.adapter;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.er1ck.consis.R;



public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {
    private Cursor mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView lastname;
        public TextView direction;
        public TextView tel;
        public TextView email;
        public TextView birthday;
        public TextView state;
        public TextView nickname;
        public TextView password;

        public ViewHolder(View v) {
            super(v);
            name = (TextView)v.findViewById(R.id.name);
            lastname = (TextView)v.findViewById(R.id.lastname);
            direction = (TextView)v.findViewById(R.id.direction);
            tel = (TextView)v.findViewById(R.id.tel);
            email = (TextView)v.findViewById(R.id.email);
            birthday = (TextView)v.findViewById(R.id.birhday);
            state = (TextView)v.findViewById(R.id.state);
            nickname = (TextView)v.findViewById(R.id.nickname);
            password = (TextView)v.findViewById(R.id.password);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PersonAdapter(Cursor c) {
        mDataset = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PersonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        mDataset.moveToPosition(position);
        holder.name.setText(mDataset.getString(1));
        holder.lastname.setText(mDataset.getString(2));
        holder.direction.setText(mDataset.getString(3));
        holder.tel.setText(mDataset.getString(4));
        holder.email.setText(mDataset.getString(5));
        holder.birthday.setText(mDataset.getString(6));
        holder.state.setText(mDataset.getString(7));
        holder.nickname.setText(mDataset.getString(8));
        holder.password.setText(mDataset.getString(9));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.getCount();
    }
}

