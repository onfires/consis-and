package com.er1ck.consis;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.er1ck.consis.entity.Person;
import com.er1ck.consis.service.PersonContract.PersonEntry;
import com.er1ck.consis.service.PersonService.PersonReaderDbHelper;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {

    TextView name;
    TextView lastName;
    TextView direction;
    TextView tel;
    TextView email;
    TextView birthday;
    TextView state;
    TextView nickname;
    TextView password;

    Button btnSend;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    String dateInString = "7-Jun-2013";

    private PersonReaderDbHelper mDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name        = (TextView)findViewById(R.id.name);
        lastName    = (TextView)findViewById(R.id.lastname);
        direction   = (TextView)findViewById(R.id.direction);
        tel         = (TextView)findViewById(R.id.tel);
        email       = (TextView)findViewById(R.id.email);
        birthday    = (TextView)findViewById(R.id.birthday);
        state       = (TextView)findViewById(R.id.state);
        nickname    = (TextView)findViewById(R.id.nickname);
        password    = (TextView)findViewById(R.id.password);

        btnSend    = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);

        this.mDbHelper = new PersonReaderDbHelper(this);

        Cursor persons = readPersons();

        if (persons.getCount() <= 0){
            saveDummyPerson();
        }

        try {
            while (persons.moveToNext()) {
                String name = persons.getString(1);
                Log.e("NAME", String.format("name is::%s",name));
            }
        } finally {
            persons.close();
        }

    }

    @Override
    public void onClick(View v) {

        if (v == btnSend){
            Log.i("CLICK", "Send data");
            if ( isValid()){
                long newRowId = save(fill());
                if ( newRowId > 0){
                    Log.i("CLICK", "User Create");
                    Intent intent = new Intent(this, ListActivity.class);
                    startActivity(intent);
                    Toast toast = Toast.makeText(getApplicationContext(), String.format("Person %s create id: %d",name.getText().toString(), newRowId), Toast.LENGTH_LONG);
                    toast.show();
                }
            }else {
                Toast toast = Toast.makeText(getApplicationContext(), String.format("Verifica los campos"), Toast.LENGTH_SHORT);
                toast.show();
            }
        }

    }

    private Person fill (){

        Date date = new Date();
        try {

            date = this.formatter.parse(dateInString);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Person p =  new Person(
                name.getText().toString(),
                lastName.getText().toString(),
                direction.getText().toString(),
                tel.getText().toString(),
                email.getText().toString(),
                date,
                state.getText().toString(),
                nickname.getText().toString(),
                password.getText().toString()
        );

        return p;
    }

    private Boolean isValid(){
        Boolean isValid = true;

        if (!isValidText(name.getText().toString())) {
            name.setError("Invalid Name");
            isValid = false;
        }

        if (!isValidText(lastName.getText().toString())) {
            lastName.setError("Invalid Last Name");
            isValid = false;
        }

        if (!isValidText(direction.getText().toString())) {
            direction.setError("Invalid Direction");
            isValid = false;
        }

        if (!isValidNumber(tel.getText().toString())) {
            tel.setError("Invalid Tel");
            isValid = false;
        }

        if (!isValidEmail(email.getText().toString())) {
            email.setError("Invalid Email");
            isValid = false;
        }
        //falta fecha de nacieiento
        /*if (!isValidText(direction.getText().toString())) {
            direction.setError("Invalid Direction");
        }*/

        if (!isValidText(state.getText().toString())) {
            state.setError("Invalid State");
            isValid = false;
        }

        if (!isValidText(nickname.getText().toString())) {
            nickname.setError("Invalid Nickname");
            isValid = false;
        }

        if (!isValidPassword(password.getText().toString())) {
            password.setError("Invalid Password");
            isValid = false;
        }



        return isValid;
    }

    private void saveDummyPerson(){
        // Gets the data repository in write mode
        Date date = new Date();
        Person p = new Person("name","last_name","direction","tel","email", date,"state","nickname","password");

        long newRowId = save(p);

        Toast toast = Toast.makeText(getApplicationContext(), String.format("Dummy Person Ceate id: %d",newRowId), Toast.LENGTH_SHORT);
        toast.show();
        //Log.d()
    }

    private long save(Person p){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(PersonEntry.COLUMN_NAME_NAME         , p.name);
        values.put(PersonEntry.COLUMN_NAME_LAST_NAME    , p.lastName);
        values.put(PersonEntry.COLUMN_NAME_DIRECTION    , p.direction);
        values.put(PersonEntry.COLUMN_NAME_TEL          , p.tel);
        values.put(PersonEntry.COLUMN_NAME_EMAIL        , p.email);
        values.put(PersonEntry.COLUMN_NAME_BIRTHDAY     , p.birthday.toString());
        values.put(PersonEntry.COLUMN_NAME_STATE        , p.state);
        values.put(PersonEntry.COLUMN_NAME_NICKNAME     , p.nickname);
        values.put(PersonEntry.COLUMN_NAME_PASSWORD     , p.password);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(PersonEntry.TABLE_NAME, null, values);



        //return newRowId;
    }

    private Cursor readPersons(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                PersonEntry._ID,
                PersonEntry.COLUMN_NAME_NAME,
                PersonEntry.COLUMN_NAME_LAST_NAME,
                PersonEntry.COLUMN_NAME_DIRECTION,
                PersonEntry.COLUMN_NAME_TEL,
                PersonEntry.COLUMN_NAME_EMAIL,
                PersonEntry.COLUMN_NAME_BIRTHDAY,
                PersonEntry.COLUMN_NAME_STATE,
                PersonEntry.COLUMN_NAME_NICKNAME,
                PersonEntry.COLUMN_NAME_PASSWORD,
        };

        // Filter results WHERE "title" = 'My Title'
        //String selection = PersonEntry.COLUMN_NAME_TITLE + " = ?";
        //String[] selectionArgs = { "My Title" };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                PersonEntry._ID + " ASC";

        Cursor c = db.query(
                PersonEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,//selection,                                // The columns for the WHERE clause
                null,//selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return c;
    }

    //Validations

    // validating text id
    private boolean isValidText(String text) {
        String EMAIL_PATTERN = "[_ A-Za-z]+";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    // validating number id
    private boolean isValidNumber(String text) {

        if (text == null && text.length() < 8) {
            return false;
        }

        String EMAIL_PATTERN = "[0-9]+";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }
}
