package com.er1ck.consis.entity;

import java.util.Date;

/**
 * Created by aplicacionesmoviles on 19/06/17.
 */

public class Person {

    public Person(){}
    public Person(String name,String  lastName,String  direction,String  tel,String  email,Date birthday,String state,String nickname,String password){
        this.name= name;
        this.lastName= lastName;
        this.direction= direction;
        this.tel=         tel;
        this.email= email;
        this.birthday= birthday;
        this.state= state;
        this.nickname= nickname;
        this.password= password;
    }

    public String name;
    public String lastName;
    public String direction;
    public String tel;
    public String email;
    public Date   birthday;
    public String state;
    public String nickname;
    public String password;

}
